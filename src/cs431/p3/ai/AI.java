package cs431.p3.ai;

import java.util.ArrayList;
import java.util.Collections;
import cs431.p3.model.Board;

/**
 * Class to represent an Artificial Intelligence that will
 * solve a Connect 4 problem using Alpha-Beta pruning.
 * @author John Salis, Christian Micklisch, Nathan Hilliard
 */
public class AI extends Connect4AI {
	/**
	 * The variable TREE_DEPTH will help us in creating 
	 * the tree which will be used to control all of the 
	 * decisions that the AI will make.
	 * @author Christian Micklisch
	 */
	public static final int TREE_DEPTH = 6;
	private int myPlayerId;
	private int myMove;
	
	/**
	 * Constructor.
	 * @param playerId The ID of the player (1 for Player 1, 2 for Player 2).
	 */
	public AI(int playerId) {
		this.myPlayerId = playerId;
	}

	/**
	 * To decide what the player needs to do he first receives 
	 * the boards state. 
	 * 
	 * After the tree of information has been built with an appropriate
	 * cost for each node we will then go on to prune the tree.
	 * 
	 * Once that has been done the AI will then make the decision based
	 * off of the pruned tree.
	 * @param board The state of the board.
	 * @return The column that the move was made in.
	 * @author Christian Micklisch
	 */
	public int getMove(int[][] board)
	{
		double value = maxValue(new State(board), Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
		System.out.println("Player : " + myPlayerId + " :: Alpha-Beta : " + value);
		return this.myMove;
	}
	
	/**
	 * Determines the max value.
	 * @param state The state to consider.
	 * @param alpha The alpha values.
	 * @param beta The beta values.
	 * @return The max value.
	 */
	private double maxValue(State state, double alpha, double beta)
	{
		ArrayList<Integer> actionList = getActionList(state.getBoardState());
		double cost = AI.calcHeuristic(state.getBoardState(), myPlayerId);
		// Test for a terminal state
		if (state.getDepth() == AI.TREE_DEPTH || actionList.isEmpty() ||
			cost == Double.POSITIVE_INFINITY || cost == Double.NEGATIVE_INFINITY) {
			return cost;
		}
		// Initialize the default move
		if (state.getDepth() == 0) {
			this.myMove = actionList.get(0);
		}
		double value = Double.NEGATIVE_INFINITY;
		for (Integer action : actionList) {
			State newState = state.applyAction(this.myPlayerId, action);
			double newValue = minValue(newState, alpha, beta);
			if (newValue > value) {
				value = newValue;
				if (state.getDepth() == 0) {
					this.myMove = newState.getAction();
				}
			}
			alpha = Math.max(alpha, value);
			if (alpha >= beta && state.getDepth() != 0)
				return value;
		}
		return value;
	}
	
	/**
	 * Determines the min value.
	 * @param state The state we're calculating for.
	 * @param alpha The alpha value.
	 * @param beta The beta value.
	 * @return The min value.
	 */
	private double minValue(State state, double alpha, double beta)
	{
		ArrayList<Integer> actionList = getActionList(state.getBoardState());
		double cost = AI.calcHeuristic(state.getBoardState(), myPlayerId);
		// Test for a terminal state
		if (state.getDepth() == AI.TREE_DEPTH || actionList.isEmpty() ||
			cost == Double.POSITIVE_INFINITY || cost == Double.NEGATIVE_INFINITY) {
			return cost;
		}
		double value = Double.POSITIVE_INFINITY;
		for (Integer action : actionList) {
			State newState = state.applyAction(getOpponentId(), action);
			value = Math.min(value, maxValue(newState, alpha, beta));
			beta = Math.min(beta, value);
			if (beta <= alpha)
				return value;
		}
		return value;
	}
	
	/**
	 * Generates an ordered list of legal actions for use by the minimax search.
	 * @param board The board state.
	 * @return A list of legal actions for the board state.
	 */
	private static ArrayList<Integer> getActionList(int[][] board) {
		boolean[] isLegal = Board.getLegalActions(board);
		ArrayList<Integer> actionList = new ArrayList<Integer>();
		for (int action = 0; action < isLegal.length; action++) {
			if (isLegal[action]) {
				actionList.add(action);
			}
		}
		// Collections.shuffle(actionList);
		return actionList;
	}
	
	/**
	 * @return The ID of the opponent player.
	 */
	private int getOpponentId() {
		if (this.myPlayerId == Board.PLAYER_1)
			return Board.PLAYER_2;
		else 
			return Board.PLAYER_1;
	}
	
	/**
	 * Calculates the heuristic of the board with respect to a given player ID.
	 * A larger value is better for that player. Calculation is performed by checking 
	 * the winning line in every direction and from every point on the board.
	 * An opponent winning line penalizes the cost.
	 * @param board The board state.
	 * @param playerId The player to calculate the heuristic for.
	 * @return The total cost of the board state.
	 * 
	 * @author John Salis
	 */
	public static double calcHeuristic(int[][] board, int playerId) {
		double totalCost = 0;
		for (int i = 0; i < Board.HEIGHT; i++) {
			for (int j = 0; j < Board.WIDTH; j++) {
				totalCost += calcHeuristicAt(board, playerId, i, j, 0, 1);
				totalCost += calcHeuristicAt(board, playerId, i, j, 1, 0);
				totalCost += calcHeuristicAt(board, playerId, i, j, 1, 1);
				totalCost += calcHeuristicAt(board, playerId, i, j, -1, 1);
				
				if (totalCost == Double.POSITIVE_INFINITY || 
					totalCost == Double.NEGATIVE_INFINITY) {
					return totalCost;
				}
			}
		}
		return totalCost;
	}
	
	/**
	 * Checks for a possible winning line starting a given board cell, and
	 * scanning in a given direction. A line is a winning line for a player
	 * if it contains at least one of that player's pieces and none of the
	 * opponent's pieces. Winning lines with more filled in pieces have higher
	 * weight.
	 * @param board The board state.
	 * @param playerId The player to calculate the heuristic for.
	 * @param i The row to start the scan at.
	 * @param j The column to start the scan at.
	 * @param rowDir The row direction to scan in. Domain: {-1, 0, 1}
	 * @param colDir The column direction to scan in. Domain: {-1, 0, 1}
	 * @return The calculated cost at position i, j for the given player ID.
	 * 
	 * @author John Salis
	 */
	private static double calcHeuristicAt(int[][] board, int playerId, int i, int j, int rowDir, int colDir) {
		int series = 0;
		int check = Board.EMPTY_CELL;
		for (int step = 0; step < Board.WINNING_LINE_LENGTH; step++) {
			if (!Board.isValid(i + (step * rowDir), j + (step * colDir)))
				return 0;
			
			int current = board[i + (step * rowDir)][j + (step * colDir)];
			if (check == Board.EMPTY_CELL)
				check = current;
			
			if (check != Board.EMPTY_CELL && current != Board.EMPTY_CELL) {
				if (current == check)
					series ++;
				else
					return 0;
			}
		}
		
		// Calculates the cost. A longer series has a higher weight.
		double cost;
		if (series == Board.WINNING_LINE_LENGTH)
			cost = Double.POSITIVE_INFINITY;
		else
			cost = series * Math.pow(10, series - 1);
		
		// If the series belongs to another player, then the cost is negative.
		if (check != playerId)
			cost = -cost;
		
		return cost;
	}
}
