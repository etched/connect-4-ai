package cs431.p3.ai;

/**
 * Interface for which an agent for the Connect 4 game must implement in order to function.
 * @author Nathan Hilliard
 */
public abstract class Connect4AI {
	/**
	 * Function must return a valid column of the board following its decided move.
	 * @param board The current state of the game board.
	 * @return The column of the game board to make a move on.
	 */
	public abstract int getMove(int[][] board);
}