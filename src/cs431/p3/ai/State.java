package cs431.p3.ai;

import cs431.p3.model.Board;

/**
 * 
 * The State class is used to store the Depth at which the Node
 * is at, the current board state, and then also the column the
 * AI would choose.
 * 
 * @author Christian Micklisch
 */

public class State {
	/** The depth of this state. */
	private int myDepth;
	
	/** This states action. */
	private int myAction;
	
	/** The value of this state. */
	private double myValue;
	
	/** The board state, stored as a 2D int array.*/
	private int[][] myBoardState;
	
	/**
	 * The default constructor.
	 */
	public State() {
		this.myDepth = 0;
		this.myAction = 0;
	}
	
	/**
	 * Initialize the state using a game board.
	 * @param boardState A 2D int array representing a board state.
	 */
	public State(int[][] boardState) {
		this.myDepth = 0;
		this.myAction = 0;
		this.myBoardState = boardState;
	}
	
	/**
	 * More specific constructor.
	 * @param depth The depth to traverse to.
	 * @param move The column that would be chosen.
	 * @param boardState The state of the board.
	 */
	public State(int depth, int move, int[][] boardState) {
		this.myDepth = depth;
		this.myAction = move;
		this.myBoardState = boardState;
	}
	
	/**
	 * Applies a player action to the board state then returns
	 * a new state.
	 * new state is generated.
	 * @param player The player to apply the action for.
	 * @param action The action (board column) to take.
	 */
	public State applyAction(int player, int action) {
		int[][] newBoard = Board.copyBoard(this.myBoardState);
		Board.makeMove(newBoard, player, action);
		return new State(this.myDepth + 1, action, newBoard);
	}
	
	/*
	 * Getters and Setters
	 */
	
	/**
	 * @return Returns the depth.
	 */
	public int getDepth() {
		return this.myDepth;
	}
	
	/**
	 * @return Returns the action.
	 */
	public int getAction() {
		return this.myAction;
	}
	
	/**
	 * @return Returns the board state.
	 */
	public int[][] getBoardState() {
		return this.myBoardState;
	}
	
	/**
	 * @return This value of this state.
	 */
	public double getValue() {
		return this.myValue;
	}
	
	/**
	 * Setters
	 */
	
	/**
	 * @param depth The new depth.
	 */
	public void setDepth(int depth) {
		this.myDepth = depth;
	}
	
	/**
	 * @param action The new action.
	 */
	public void setAction(int action) {
		this.myAction = action;
	}
	
	/**
	 * @param boardState The new board state.
	 */
	public void setBoardState(int[][] boardState) {
		this.myBoardState = boardState;
	}
	
	/**
	 * @param value The new value.
	 */
	public void setValue(double value) {
		this.myValue = value;
	}
}
