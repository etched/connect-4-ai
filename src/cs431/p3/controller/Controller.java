package cs431.p3.controller;

import cs431.p3.model.Board;
import cs431.p3.model.Model;
import cs431.p3.GUI.*;

/**
 * The controller component of the application.
 * @author Nathan Hilliard, Kyle Campbell, and Jure Jumalon
 */
public class Controller {
	/** Instance of the model. */
	private Model myModel;
	
	/** Instance of the view. */
	private Connect4GUI myFrame;
	
	/**
	 * Default constructor.
	 */
	public Controller() {
		// Initialize all of the properties and set the frame to be visible.
		this.myModel = new Model();
		this.myFrame = new Connect4GUI(this);
		this.myFrame.setVisible(true);
	}
	
	/**
	 * Starts the game with the selected player going first.
	 */
	public void startGame() {
		// Reinitialize the model when the startGame button is pressed so that if another game wants to be played
		// we get a fresh start.
		this.myModel = new Model();
		
		// Refresh the GUI.
		this.update();
		
		int player = this.myFrame.getStartingPlayer();
		this.myModel.startMatch(player);
		this.printLog("Player " + player + " starts the game.");
	}
	
	/**
	 * Makes the next move and then updates the GUI.
	 */
	public void nextMove() {
		int player = this.myModel.getCurrentPlayer();
		boolean turn = this.myModel.performTurn();
		if (turn) {
			int col = this.myModel.getLastMove();
			this.printLog("AI Player " + player + " places a token in column " + col + ".");
			this.update();
		}

		int winner = this.myModel.getWinner();
		if (winner != 0) {
			this.printLog("Player " + winner + " has won the game!");
			return;
		}
		
		// Check for a tie.
		int[][] board = this.myModel.getBoard();
		for (int j = 0; j < Board.WIDTH; j++)
			if (board[0][j] == 0)
				return;
		
		this.printLog("It's a tie!");
	}
	
	/**
	 * This simply takes in the current board state and updates the GUI with the correct information. 
	 */
	public void update() {
		int[][] board = this.myModel.getBoard();
		for (int i = 0; i < board.length; i++)
			for (int j = 0; j < board[i].length; j++)
				this.myFrame.updateCell(i, j, board[i][j]);
	}
	
	/**
	 * Makes the currently active player place a token into the specified column
	 * @param col The column to make the move in.
	 * @author Jure Jumalon
	 */
	public void makeMove(Integer col) {
		int player = this.myModel.getCurrentPlayer();
		boolean turn = this.myModel.performTurn(col);
		if (turn) {
			this.printLog("Human Player " + player + " places a token in column " + col + ".");
			this.update();
		}

		int winner = this.myModel.getWinner();
		if (winner != 0) {
			this.printLog("Player " + winner + " has won the game!");
			return;
		}
		
		// Check for a tie.
		int[][] board = this.myModel.getBoard();
		for (int j = 0; j < Board.WIDTH; j++)
			if (board[0][j] == 0)
				return;
		
		this.printLog("It's a tie!");
	}

	/**
	 * Prints a message to the log in the UI.
	 */
	public void printLog(String msg) {
		this.myFrame.updateLog(msg);
	}
}
