package cs431.p3.controller;

/**
 * Class to house the main application entry point.
 * @author Nathan Hilliard
 */
public class Main {
	public static void main(String[] args) {
		new Controller();
	}
}
