package cs431.p3.junit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cs431.p3.model.Board;
import cs431.p3.model.Model;

/**
 * Class to test the Board class.
 * @author Nathan Hilliard
 */
public class BoardTest {
	/** The board we're testing. */
	private Board myBoard;

	@Before
	public void setUp() throws Exception {
		this.myBoard = new Board();
	}

	/**
	 * Test to ensure we can pick up a win in a row.
	 * @author Nathan Hilliard & John Salis
	 */
	@Test
	public void testGetRowState() {
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 2);
		this.myBoard.makeMove(Board.PLAYER_1, 3);
		
		assertTrue(this.myBoard.getRowState(5) == 1);
	}
	
	/**
	 * Test to ensure we can pick up a win in a column.
	 * @author Nathan Hilliard & John Salis
	 */
	@Test
	public void testGetColumnState() {
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		
		assertTrue(this.myBoard.getColumnState(1) == 1);
		assertTrue(this.myBoard.getColumnState(0) == 0);
	}

	/**
	 * Test if we can correctly assess the state of a diagonal on
	 * the game board.
	 * @author Nathan Hilliard
	 */
	@Test
	public void testGetDiagonalState() {
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 2);
		this.myBoard.makeMove(Board.PLAYER_2, 3);
		this.myBoard.makeMove(Board.PLAYER_2, 2);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_2, 1);
		this.myBoard.makeMove(Board.PLAYER_2, 0);
		
		assertTrue(this.myBoard.getDiagonalState(5, 3, 2, 0) == 2);
	}
	
	/**
	 * Test to ensure that win-condition checking functions properly.
	 * @author Nathan Hilliard
	 */
	@Test
	public void testIsSolved() {
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 2);
		this.myBoard.makeMove(Board.PLAYER_2, 3);
		this.myBoard.makeMove(Board.PLAYER_2, 2);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_2, 1);
		this.myBoard.makeMove(Board.PLAYER_2, 0);
		assertTrue(this.myBoard.isSolved() == 2);
		
		this.myBoard = new Board();
		this.myBoard.makeMove(Board.PLAYER_1, 6);
		this.myBoard.makeMove(Board.PLAYER_1, 5);
		this.myBoard.makeMove(Board.PLAYER_1, 4);
		this.myBoard.makeMove(Board.PLAYER_1, 5);
		this.myBoard.makeMove(Board.PLAYER_1, 6);
		this.myBoard.makeMove(Board.PLAYER_1, 6);
		
		this.myBoard.makeMove(Board.PLAYER_2, 4);
		this.myBoard.makeMove(Board.PLAYER_2, 5);
		this.myBoard.makeMove(Board.PLAYER_2, 3);
		this.myBoard.makeMove(Board.PLAYER_2, 6);
		assertTrue(this.myBoard.isSolved() == 2);
	}
	
	/**
	 * Noticed an error that occurred with a certain board state, tried to see 
	 * if it might have been the Board class.
	 * @author John Salis, Christian Micklisch
	 */
	@Test
	public void testFullBoardPotentialFailureState() {
		this.myBoard.makeMove(Board.PLAYER_2, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_2, 0);
		this.myBoard.makeMove(Board.PLAYER_1, 0);
		this.myBoard.makeMove(Board.PLAYER_2, 0);
		
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_2, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_1, 1);
		this.myBoard.makeMove(Board.PLAYER_2, 1);
		
		this.myBoard.makeMove(Board.PLAYER_1, 2);
		this.myBoard.makeMove(Board.PLAYER_2, 2);
		this.myBoard.makeMove(Board.PLAYER_2, 2);
		this.myBoard.makeMove(Board.PLAYER_2, 2);
		this.myBoard.makeMove(Board.PLAYER_1, 2);
		this.myBoard.makeMove(Board.PLAYER_1, 2);
		
		this.myBoard.makeMove(Board.PLAYER_1, 3);
		this.myBoard.makeMove(Board.PLAYER_2, 3);
		this.myBoard.makeMove(Board.PLAYER_1, 3);
		this.myBoard.makeMove(Board.PLAYER_1, 3);
		this.myBoard.makeMove(Board.PLAYER_2, 3);
		this.myBoard.makeMove(Board.PLAYER_2, 3);
		
		this.myBoard.makeMove(Board.PLAYER_2, 4);
		this.myBoard.makeMove(Board.PLAYER_2, 4);
		this.myBoard.makeMove(Board.PLAYER_1, 4);
		this.myBoard.makeMove(Board.PLAYER_2, 4);
		this.myBoard.makeMove(Board.PLAYER_2, 4);
		this.myBoard.makeMove(Board.PLAYER_1, 4);
		
		this.myBoard.makeMove(Board.PLAYER_1, 6);
		this.myBoard.makeMove(Board.PLAYER_2, 6);
		this.myBoard.makeMove(Board.PLAYER_1, 6);
		this.myBoard.makeMove(Board.PLAYER_2, 6);
		this.myBoard.makeMove(Board.PLAYER_1, 6);
		this.myBoard.makeMove(Board.PLAYER_2, 6);
		
		assertTrue(myBoard.isSolved() == 0);
	}
}
