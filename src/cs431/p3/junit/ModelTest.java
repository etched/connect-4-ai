package cs431.p3.junit;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import cs431.p3.model.Model;

/**
 * JUnit test cases for the Model.
 * @author Nathan Hilliard
 */
public class ModelTest {
	/** The model we'll test. */
	private Model myModel;
	
	@Before
	public void setUp() throws Exception {
		// Initialize the object that is to be tested.
		this.myModel = new Model();
	}
	
	/**
	 * A test to ensure the Model correctly keeps track of the last move
	 * played in the game.
	 */
	@Test
	public void testGetLastMove() {
		this.myModel.performTurn(0);
		assertTrue(this.myModel.getLastMove() == 0);
	}
	
	/**
	 * Assure that when the board is reset we also update the last move made.
	 */
	@Test
	public void testResetBoard() {
		this.myModel.performTurn(2);
		this.myModel.resetBoard();
		assertTrue(this.myModel.getLastMove() == Model.ERROR);
	}
}
