package cs431.p3.junit;

import static org.junit.Assert.*;
import org.junit.Test;
import cs431.p3.ai.AI;
import cs431.p3.model.Board;

/**
 * Test suite for AI functions.
 * @author John Salis & Christian Micklisch
 */
public class AITest {
	/*
	 * Checks if move was performed in the proper location
	 */
	@Test
	public void testPerformTurn() {
		Board board = new Board();
		
		board.makeMove(Board.PLAYER_1, 0);
		board.makeMove(Board.PLAYER_2, 0);
		assertTrue(board.getCell(5, 0) == Board.PLAYER_1);
		assertTrue(board.getCell(4, 0) == Board.PLAYER_2);
	}
	
	/*
	 * check 1st move
	 */
	
	/**
	 * These tests check to see what the cost is of the Player and the Opponent
	 * after a certain amount of move sets, the costs themselves should follow a
	 * pattern based off of the heuristic.
	 * 
	 * @author John Salis, Christian Micklisch
	 */
	
	/*
	 * This test is here to check the cost after 2 moves from each player
	 * 
	 * When the players have both the same number of winning lines then the cost is
	 * zero.
	 * 
	 * When the 1st player puts a piece in the center board then the cost is 7.
	 * 
	 * If the player has a winning move then the cost is equal to infinity for player,
	 * and negative infinity for the opponent.
	 */
	@Test
	public void testCostTwoMoveHorizontal() {
		// Checks a basic same cost system.
		
		Board board = new Board();
		
		board.makeMove(Board.PLAYER_1, 0);
		assertTrue(3 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_1));
		assertTrue(-3 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_2));
		
		board.makeMove(Board.PLAYER_2, 6);
		assertTrue(0 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_1));
		assertTrue(0 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_2));
		
		// Checks the 7 cost instance.
		
		board = new Board();

		board.makeMove(Board.PLAYER_1, 3);
		assertTrue(7 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_1));
		assertTrue(-7 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_2));
		
		// Checks the infinity instance.
		
		board = new Board();

		board.makeMove(Board.PLAYER_1, 3);
		board.makeMove(Board.PLAYER_2, 4);
		board.makeMove(Board.PLAYER_1, 2);
		board.makeMove(Board.PLAYER_2, 5);
		board.makeMove(Board.PLAYER_1, 1);
		board.makeMove(Board.PLAYER_2, 6);
		board.makeMove(Board.PLAYER_1, 0);
		
		assertTrue(Double.POSITIVE_INFINITY == AI.calcHeuristic(board.getBoard(), Board.PLAYER_1));
		assertTrue(Double.NEGATIVE_INFINITY == AI.calcHeuristic(board.getBoard(), Board.PLAYER_2));
	}
	
	@Test
	public void testCostTwoMoveVertical() {
		// Checks a basic same cost system.
		Board board = new Board();
		
		board.makeMove(Board.PLAYER_1, 0);
		board.makeMove(Board.PLAYER_2, 6);
		board.makeMove(Board.PLAYER_1, 0);
		assertTrue(22 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_1));
		assertTrue(-22 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_2));
		
		// Checks the infinity instance.
		
		board = new Board();

		board.makeMove(Board.PLAYER_1, 0);
		board.makeMove(Board.PLAYER_2, 6);
		board.makeMove(Board.PLAYER_1, 0);
		board.makeMove(Board.PLAYER_2, 6);
		board.makeMove(Board.PLAYER_1, 0);
		board.makeMove(Board.PLAYER_2, 6);
		board.makeMove(Board.PLAYER_1, 0);
		
		assertTrue(Double.POSITIVE_INFINITY == AI.calcHeuristic(board.getBoard(), Board.PLAYER_1));
		assertTrue(Double.NEGATIVE_INFINITY == AI.calcHeuristic(board.getBoard(), Board.PLAYER_2));
	}
	
	@Test
	public void testCostTwoMoveDiagonal() {
		// Checks a basic same cost system.
		Board board = new Board();

		board.makeMove(Board.PLAYER_1, 0);
		board.makeMove(Board.PLAYER_2, 1);
		board.makeMove(Board.PLAYER_1, 1);
		assertTrue(23 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_1));
		assertTrue(-23 == AI.calcHeuristic(board.getBoard(), Board.PLAYER_2));

		// Checks the infinity instance.

		board = new Board();

		board.makeMove(Board.PLAYER_1, 0);
		board.makeMove(Board.PLAYER_2, 1);
		board.makeMove(Board.PLAYER_1, 1);
		board.makeMove(Board.PLAYER_2, 2);
		board.makeMove(Board.PLAYER_1, 3);
		board.makeMove(Board.PLAYER_2, 2);
		board.makeMove(Board.PLAYER_1, 2);
		board.makeMove(Board.PLAYER_2, 3);
		board.makeMove(Board.PLAYER_1, 4);
		board.makeMove(Board.PLAYER_2, 3);
		board.makeMove(Board.PLAYER_1, 3);
		
		/*
		 * check 4 connected (Diagonal, Vertical, Horizontal)
		 * 
		 * For both the Player and opponent they should have a cost of
		 * positive and negative infinity respectively 
		 */
		assertTrue(Double.POSITIVE_INFINITY == AI.calcHeuristic(board.getBoard(), Board.PLAYER_1));
		assertTrue(Double.NEGATIVE_INFINITY == AI.calcHeuristic(board.getBoard(), Board.PLAYER_2));
	}
}
