package cs431.p3.model;

/**
 * Class which represents the connect-4 game board with values for each player.
 * @author Nathan Hilliard
 */
public class Board {
	/** The width of the board, traditionally 7. */
	public static final int WIDTH = 7;
	
	/** The height of the board, traditionally 6. */
	public static final int HEIGHT = 6;
	
	/** Definitions for Player 1 and 2. */
	public static final int PLAYER_1 = 1;
	public static final int PLAYER_2 = 2;
	
	/** Defines an open space on the Connect 4 board. */
	public static final int EMPTY_CELL = 0;
	
	/** Defines the number of connecting pieces to win */
	public static final int WINNING_LINE_LENGTH = 4;
	
	/** Our board state with 1's and 2's denoting ownership
	 *  of a position by a specific player and 0 denoting an open space. */
	private int[][] myBoard;
	
	/**
	 * Constructor.
	 */
	public Board() {
		this.myBoard = new int[Board.HEIGHT][Board.WIDTH];
		
		// Initialize all the cells of the board to 0.
		for (int i = 0; i < Board.HEIGHT; i++)
			for (int j = 0; j < Board.WIDTH; j++)
				this.myBoard[i][j] = Board.EMPTY_CELL;
	}
	
	/**
	 * A function to determine if the Connect 4 game is solved.
	 * @return The winner (1 or 2) or 0 if there is no winner.
	 */
	public int isSolved() {
		int cur = Board.EMPTY_CELL;
		// 1. Check rows
		for (int i = 0; i < Board.HEIGHT; i++) {
			cur = this.getRowState(i);
			if (cur != Board.EMPTY_CELL)
				return cur;
		}
		
		// 2. Check columns
		for (int j = 0; j < Board.WIDTH; j++) {
			cur = this.getColumnState(j);
			if (cur != Board.EMPTY_CELL)
				return cur;
		}
		
		// 3. Check backwards facing diagonals "\"
		int i = Board.HEIGHT - 2;
		int j = 1;
		for (; i >= 0 && j < Board.WIDTH;) {
			cur = this.getDiagonalState(i,0 , 5,j);
			if (cur != Board.EMPTY_CELL)
				return cur;

			if (i != 0 && j+1 != 6) {
				cur = this.getDiagonalState(i,6 , 0,j+1);
				if (cur != Board.EMPTY_CELL)
					return cur;
			}

			i--;
			j++;
		}

		// This case is skipped by the loop above so check it here.
		cur = this.getDiagonalState(0,1 , 5,6);
		if (cur != Board.EMPTY_CELL)
			return cur;

		// 4. Check forwards facing diagonals "/"
		i = Board.HEIGHT - 2;
		j = Board.HEIGHT - 2;
		for (; i >= 0 && j >= 0;)
		{
			cur = this.getDiagonalState(i,6 , 5,j);
			if (cur != Board.EMPTY_CELL)
				return cur;

			i--;
			j--;
		}

		i = 5;
		j = 5;
		for (; i > 0 && j >= 0;)
		{
			cur = this.getDiagonalState(i,0 , 0,j);
			if (cur != Board.EMPTY_CELL)
				return cur;

			i--;
			j--;
		}

		// Nothin' found? No winner!
		return Board.EMPTY_CELL;
	}
	
	
	/**
	 * Returns what win-state the column is in.
	 * @param col The column to check the state of.
	 * @return
	 * 		0 - No winner
	 * 		1 - Player 1 is the winner
	 * 		2 - Player 2 is the winner
	 * @see
	 * 		{@link Model#EMPTY_CELL}, {@link Model#PLAYER_1}, 
	 * 			{@link Model#PLAYER_2}
	 */
	public int getColumnState(int col) {
		// Designates who we are currently
		int me = Board.EMPTY_CELL;
		
		// Designates how many consecutive vertical entries
		// we've come across.
		int consecutive = 0;
		
		for (int i = 0; i < Board.HEIGHT; i++) {
			// If our current position is also a cell owned by our
			// current player, increment the number of consecutive
			// cells owned by us.
			if (this.myBoard[i][col] == me)
				consecutive++;
			
			// Our current position is owned by a different player
			// so we update who we are and reset the number of consecutive
			// cells.
			else {
				me = this.myBoard[i][col];
				consecutive = 1;
			}
			
			// If we are a player and have four in a row then we're a winner!
			if (me != Board.EMPTY_CELL && consecutive == Board.WINNING_LINE_LENGTH)
				return me;
		}
		
		// No solution here.
		return 0;
	}
	
	
	/**
	 * See {@link Board#getColumnState(int)} for documentation as its purpose is the same
	 * just searches in a different direction.
	 */
	public int getRowState(int row) {
		int me = Board.EMPTY_CELL;
		int consecutive = 0;

		for (int j = 0; j < Board.WIDTH; j++) {
			if (this.myBoard[row][j] == me)
				consecutive++;
			else {
				me = this.myBoard[row][j];
				consecutive = 1;
			}

			if (me != Board.EMPTY_CELL && consecutive == Board.WINNING_LINE_LENGTH)
				return me;
		}
		return Board.EMPTY_CELL;
	}
	
	
	/**
	 * Helper function to check the state of a diagonal.
	 * @param row1 Cell 1's row
	 * @param col1 Cell 1's column
	 * @param row2 Cell 2's row
	 * @param col2 Cell 2's column
	 */
	public int getDiagonalState(int row1, int col1, int row2, int col2) {
		int me = Board.EMPTY_CELL;
		int consecutive = 0;
		int[] diagonal = this.getDiagonal(row1, col1, row2, col2);
		
		// If the length of the diagonal array is less than necessary
		// to determine a winner then it is skipped to save time.
		if (diagonal.length < Board.WINNING_LINE_LENGTH)
			return Board.EMPTY_CELL;

		for (int i = 0; i < diagonal.length; i++) {
			if (diagonal[i] == me)
				consecutive++;
			else {
				me = diagonal[i];
				consecutive = 1;
			}
			
			if (me != Board.EMPTY_CELL && consecutive == Board.WINNING_LINE_LENGTH)
				return me;
		}
		
		return Board.EMPTY_CELL;
	}
	
	
	/**
	 * Draws a diagonal between two cells on the game board and returns it as a 1 Dimensional array of ints.
	 * @param row The row of the first cell.
	 * @param col The column of the first cell.
	 * @param row2 The row of the second cell.
	 * @param col2 The column of the second cell.
	 * @param minDiff Optimization parameter. If the difference between the two cells is less
	 * 		than this option, the function will return null immediately.
	 * @return 'null' if the two cells are on different diagonals, otherwise a
	 * 		1 Dimensional array of ints containing the diagonal.
	 */
	public int[] getDiagonal(int row, int col, int row2, int col2) {
		// The size of the array is the biggest difference + 1 to account for 0-based index.
		int[] array = new int[Math.max(Math.abs(col - col2), Math.abs(row - row2)) + 1];
		int rowOperation = 0; // What operation is performed on our row counter.
		int colOperation = 0; // What operation is performed on our column counter.
		
		// If row < row2, we increment by 1. Otherwise -1.
		if (row < row2)
			rowOperation = 1;
		else if (row > row2)
			rowOperation = -1;
		
		// Same for columns.
		if (col < col2)
			colOperation = 1;
		else if (col > col2)
			colOperation = -1;
		
		// Row and column counters.
		int i = row;
		int j = col;
		
		// 'array' position counter.
		int pos = 0;
		
		// Loop until we've gone from row,col to row2,col2
		while ((i != row2) && (j != col2)) {
			array[pos] = this.myBoard[i][j];
			
			// Increment our counters.
			i += rowOperation;
			j += colOperation;
			pos++;
		}
		
		// Include row2,col2 in the diagonal since the loop exits beforehand.
		array[pos] = this.myBoard[i][j];
		
		return array;
	}
	
	/**
	 * A non-static version of makeMove. By default, the move is
	 * applied to the board of the class.
	 * @param who Either PLAYER_1 or PLAYER_2, otherwise this function has no effect.
	 * @param col The column to add the game piece.
	 * @return true if the move is legal, false otherwise.
	 * 
	 * @author John Salis
	 */
	public boolean makeMove(int who, int col) {
		return Board.makeMove(myBoard, who, col);
	}
	
	/**
	 * Adds a player's game piece to a column of a board. The piece
	 * "drops" to the lowest available position.
	 * @param board The board to make the move on.
	 * @param who Either PLAYER_1 or PLAYER_2, otherwise this function has no effect.
	 * @param col The column to add the game piece.
	 * @return true if the move is legal, false otherwise.
	 * 
	 * @author John Salis
	 */
	public static boolean makeMove(int[][] board, int who, int col) {
		if (who != Board.PLAYER_1 && who != Board.PLAYER_2) return false;
		if (col < 0 || col >= Board.WIDTH) return false;
		if (board[0][col] != Board.EMPTY_CELL) return false;
		
		for (int row = 1; row < Board.HEIGHT; row ++) {
			if (board[row][col] != Board.EMPTY_CELL) {
				board[row - 1][col] = who;
				return true;
			}
		}
		board[Board.HEIGHT - 1][col] = who;
		return true;
	}
	
	/**
	 * A function to copy a 2D integer array.
	 * @param toCopy The 2D array to copy.
	 * @return A copy of the 2D array passed in as a parameter.
	 */
	public static int[][] copyBoard(int[][] toCopy) {
		int[][] board = new int[Board.HEIGHT][Board.WIDTH];
		for (int i = 0; i < Board.HEIGHT; i++) {
			for (int j = 0; j < Board.WIDTH; j++) {
				board[i][j] = toCopy[i][j];
			}
		}
		return board;
	}
	
	
	/**
	 * @return A copy of the board.
	 */
	public int[][] getBoard() {
		return Board.copyBoard(this.myBoard);
	}
	
	/**
	 * 
	 * @param i The row of the board to access.
	 * @param j The column of the board to access.
	 * @return The value contained in row i, column j of the board.
	 */
	public int getCell(int i, int j) {
		return myBoard[i][j];
	}
	
	
	/**
	 * 
	 * @param i The row of the board to check
	 * @param j The column of the board to check
	 * @return true if the point is on the board, false otherwise
	 * 
	 * @author John Salis
	 */
	public static boolean isValid(int i, int j) {
		if (i >= 0 && i < Board.HEIGHT && j >= 0 && j < Board.WIDTH)
			return true;
		else 
			return false;
	}
	
	/**
	 * Get all of the Possible moves, checking to see if a column
	 * is filled or not and then returning that as an array of columns
	 * which are open to use
	 */
	public static boolean[] getLegalActions(int[][] boardState) {
		boolean[] isLegal = new boolean[Board.WIDTH];
		for (int i = 0; i < Board.WIDTH; i++) {
			if (boardState[0][i] == Board.EMPTY_CELL)
				isLegal[i] = true;
			else
				isLegal[i] = false;
		}
		
		return isLegal;
	}
	
	
	/**
	 * Prints the board.
	 */
	public void print() {
		Board.print(this.myBoard);
	}
	
	
	/**
	 * Prints the board
	 * @param board The board to print out
	 */
	public static void print(int[][] board) {
		for (int i = 0; i < Board.HEIGHT; i++) {
			System.out.print(" | ");
			for (int j = 0; j < Board.WIDTH; j++) {
				if (board[i][j] == Board.EMPTY_CELL)
					System.out.print(". ");
				else 
					System.out.print(board[i][j] + " ");
			}
			System.out.print("|");
			System.out.println();
		}
		System.out.println(" +---------------+");
		System.out.println("//               \\\\");
	}
}
