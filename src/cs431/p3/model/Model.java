package cs431.p3.model;

import cs431.p3.ai.AI;
import cs431.p3.ai.Connect4AI;

/**
 * The model component of our application which will contain all game logic.
 * @author Nathan Hilliard
 */
public class Model {
	/** A definition for when an error occurs or something unexpected happens. */
	public static final int ERROR = -1;
	
	/** The board representing the state of the game */
	private Board myBoard;
	
	/** Our two AI players. */
	private Connect4AI myP1, myP2;
	
	/** Current player turn */
	private int myPlayerTurn;

	/** The last move made (stored as which column the move was made in) */
	private int myLastMove;
	
	/**
	 * Default constructor.
	 */
	public Model() {
		// Set the board.
		this.myBoard = new Board();
		
		// Initialize our AI players.
		this.myP1 = new AI(Board.PLAYER_1);
		this.myP2 = new AI(Board.PLAYER_2); // TODO: Swap with Team 2's AI on game day.
		
		this.myLastMove = Model.ERROR;
	}
	
	/**
	 * Begins the match.
	 * @author Kyle Campbell and Jure Jumalon
	 */
	public void startMatch(int playerTurn) {
		this.myPlayerTurn = playerTurn;
	}
	
	/**
	 * Performs the turn of the current player and switches the current player's turn
	 * @param who Either PLAYER_1 or PLAYER_2, otherwise this function has no effect.
	 * @return true if the turn was successful, false otherwise
	 * @author Kyle Campbell & Jure Jumalon
	 */
	public boolean performTurn() {
		if (this.myPlayerTurn == Board.PLAYER_1)
			return this.performTurn(this.myP1.getMove(this.myBoard.getBoard()));
		else if (this.myPlayerTurn == Board.PLAYER_2)
			return this.performTurn(this.myP2.getMove(this.myBoard.getBoard()));
		else 
			return false;
	}
	
	/**
	 * @param col The column to perform a move in.
	 * @return true if the turn was successful, false otherwise.
	 * @see {@link Model#performTurn()}
	 */
	public boolean performTurn(int col) {
		if (this.myBoard.isSolved() == 0) {
			if (this.myBoard.makeMove(myPlayerTurn, col)) {
				// If this is a legal move, update the last move 
				// made, switch turns, and output a log message.
				this.myLastMove = col;
				this.switchPlayerTurn();
				System.out.println("Legal Move : True");
				return true;
			}
			else {
				System.out.println("Legal Move : False");
				return false;
			}
		}
		
		return false;
	}
	
	/**
	 * @return The winner, or 0 if no winner exists yet.
	 */
	public int getWinner() {
		return this.myBoard.isSolved();
	}
	
	/**
	 * Private helper function to swap whose turn it is.
	 */
	private void switchPlayerTurn() {
		if (this.myPlayerTurn == Board.PLAYER_1)
			this.myPlayerTurn = Board.PLAYER_2;
		else 
			this.myPlayerTurn = Board.PLAYER_1;
	}
	
	/**
	 * @return The current game board.
	 */
	public int[][] getBoard() {
		return this.myBoard.getBoard();
	}
	
	/**
	 * Resets the game board to an empty state.
	 */
	public void resetBoard() {
		this.myBoard = new Board();
		this.myLastMove = Model.ERROR;
	}
	
	/**
	 * @return The ID of the last player to make a move.
	 */
	public int getCurrentPlayer() {
		return this.myPlayerTurn;
	}
	
	/**
	 * @return The column that the last move was made in.
	 */
	public int getLastMove() {
		return this.myLastMove;
	}
}
